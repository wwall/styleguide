#lang scribble/manual
@(require scribble/eval)

@require[@for-label[styleguide
                    racket/base
                    rackunit
                    parser-tools/lex
                    ]]

@(define my-eval (make-base-eval))
@(my-eval '(require styleguide/src/common/common
                    styleguide/src/part002/part002
                    styleguide/src/part002/main
                    ))


@title{Решение 2.}

@section{Простое определение анализатора}

Основная идея этого решения состоит в том что бы  преобразовать входящий поток символов в поток лексем.
Под лексемой понимается структура содержащая (как минимум) следующие поля -
@itemlist[
 @item{Координаты прочитанной лексемы. Для удобства анализа и вывода - в виде номера строки и колонки}
 @item{Тип лексемы.}
 @item{Текст лексемы.}]


Для того что бы написать лексический анализатор используем библиотеку @racketmodname[parser-tools/lex] и определенный в ней макрос @racket[lexer-src-pos] (так как нам необходимы лексемы с координатами).

Для определения анализатора потребуется указать типы лексем и регулярное выражение которое соответствует лексеме.

@racketblock[
 (define-lex-abbrevs
   [not-WS (:- any-char (:or #\Space #\Tab))])
 (define-tokens WSTokens (CorrectBOS inCorrectBOS))]

С помощью @racket[define-lex-abbrevs] мы определяем класс символов @racket[not-WS] который содержит в себе любой символ кроме #\Tab или #\Space.
Используя @racket[define-tokens] определяем какие лексемы будет возвращать наш анализатор. @racket[CorrectBOS] будет соответствовать верному началу строки, а @racket[inCorrectBOS] неверному.

И теперь определим анализатор следующим образом

@racketblock[
 (define BSLLang-v01
   [lexer-src-pos
    [(:seq (:+ #\Tab)) (token-CorrectBOS lexeme)]
    [(:seq (:* #\Tab) #\Space (:* (:or #\Tab #\Space))) (token-inCorrectBOS lexeme)]
    [(:+ not-WS) (return-without-pos (BSLLang-v01 input-port))]
    [(eof) eof]
    ])]

Этот код читается очень просто -
@itemlist[
 @item{Если встретили непустую последовательность из символов #\Tab - вернуть токен 'CorrectBOS и содержимое прочитанной строки}
 @item{Если встретили непустую последовательность из символов #\Tab за которой следует хотя бы один пробел и возможно пустая последовательность из пробелов и табов - вернуть токен 'inCorrectBOS и содержимое прочитанной строки}
 @item{Если прочитанная последовательность символов не содержит  пробелы или табы - вызываем анализатор рекурсивно}
 @item{Ну и наконец - если прочитали конец потока - просто вернем признак конца файла}]

Для простоты использования определим функцию @racket[tokenize-with], которая будет преобразовывать текст с помощью нужного нам анализатора.

@racketblock[

 (define (tokenize-with lexer data)
   (define input-port (cond
                        [(string? data) (parameterize ([port-count-lines-enabled #t]) (open-input-string  data))]
                        [(path? data) (parameterize ([port-count-lines-enabled #t]) (open-input-file data))]
                        [else                          data]))
   (let loop [(value (lexer input-port))
              (result '())]
     (if (eof-object? (position-token-token value))
         (begin
           (close-input-port input-port)
           (reverse result))
         (loop (lexer input-port)
               (cons value result)))))
 ]

Определение функции достаточно простое - входящий поток анализируется до тех пор пока не встретиться конец. Давайте посмотрим что вернет функция - для этого  попробуем разобрать строку @racket["  1  "] -

@interaction[#:eval my-eval
             (tokenize-with BSLLang-v01 "  1  ")]

Как видим - нам возвращается список лексем со всей нужной нам информацией (тип лексемы, ее позиция и содержание).

@section{Добавляем поддержку начала строки}

В работе нашего анализатор есть неприятная неожиданность - если пробелы до символа @racket[#\1] корректно распознаны как неверное начало строки то два пробела после @racket[#\1] по логике нашего правила вообще не должны обрабатываться (ведь они встретились не в начале строки)
@racketblock[
 (define BSLLang-v02
   [lexer-src-pos
    [(:seq (:+ #\Tab)) (if (= 0 (position-col start-pos))
                           (token-CorrectBOS lexeme)
                           (return-without-pos (BSLLang-v02 input-port)))]
    [(:seq (:* #\Tab) #\Space
           (:*
            (:or #\Tab #\Space)))           (if (= 0 (position-col start-pos))
                                                (token-inCorrectBOS lexeme)
                                                (return-without-pos (BSLLang-v02 input-port)))]
    [(:+ not-WS) (return-without-pos (BSLLang-v02 input-port))]
    [(eof) eof]
    ])
 ]

Как видите - наш анализатор это просто программа которая при нахождении нужного регулярного выражения вызывает ассоциированный с ним код. В нем  конечно нет некоторых удобств как в регулярных выражения (BOS  пришлось эмулировать) - но возможность писать свой код - это с лихвой компенсирует. 
Пока ничего загадочного и непонятного нет. А если вы знакомы с генераторами лексических анализаторов lex/flex - вам вообще все должно быть понятно.


Запускаем в REPL
@interaction[#:eval my-eval
             (tokenize-with BSLLang-v02 "  1  ")]

Все ровно так как ожидали. Давайте проверим как отработает на нашем определении тестовой строки из предыдущей части -


@interaction[#:eval my-eval
             (tokenize-with BSLLang-v02 (return-test-string))]

Анализатор возвращает только необходимые нам лексемы.  А что насчет файла? Можно ли так же легко его прочитать? Конечно же да - создадим файл и запишем него последовательность из трех строк (как ранее - первая строка только табы, две вторые - смесь из пробелов и табов). Файл разместим в каталоге test-data и назовем его rule1.bsl
@interaction[#:eval my-eval
             (with-output-to-file  #:mode 'text #:exists 'replace
               "test-data/test-rule1.bsl"
               (lambda ()  (printf (return-test-string))))
             (tokenize-with BSLLang-v02 (string->path "test-data/test-rule1.bsl"))]

Здесь первой строчкой мы выводим в файл test-rule1.bsl содержимое нашей тестовой строки, а потом уже анализируем файл (обратите внимание на вызов @racket[string->path] - он преобразует строку с именем к внутреннему представлению пути)


Теперь осталось написать проверку для правила которое будет проверять правильно ли оформлено начало строк в коде. Так как у нас есть список токенов, то мы просто применим предикат @racket[filter] с проверкой на тип лексемы 'inCorrectBOS
@racketblock[
 (define (check-token-by-rule1 token)
   (and (position-token? token)
        (eq? (token-name (position-token-token token)) 'inCorrectBOS)))
 (define (rule1 token-list)
   (filter (lambda (x) (check-token-by-rule1 x)) token-list))]

Вспомним о тесте который мы писали ранее  и приведем к виду

@interaction[#:eval my-eval
             (require  rackunit
                       parser-tools/lex
                       styleguide/src/common/common
                       styleguide/src/part002/part002)
             (define (extract-line token-list)
               (map (lambda (x) (position-line (position-token-start-pos x))) token-list))
             (check-equal? (extract-line (rule1 (tokenize-with BSLLang-v02 (return-test-string)))) '(2 3))]

Тесты не показали ошибок, значит мы можем продолжать.
Расширим действие нашего анализатора- добавим поддержку работы через командную строку.
Мне хочется чтобы анализатор вызывался примерно так @exec{styleguide -dir="путь к директории где расположен код"}, значит требуется написать модуль который будет анализировать командную строку и вызывать список проверок для всех файлов в указанной директории.
Вся логика проверки сводиться к последовательному применению @racket[BSLLang-lexer] и фильтра @racket[rule1] к каждому найденному файлу.

@racketblock[
 (define (return-file-list path)
   (find-files (lambda (x)
                 (equal? #".bsl" (path-get-extension x)))
               path))

 (define (apply-rule1 path)
   (let [(file-list (return-file-list path))]
     (filter (lambda (x) (> (length (cadr x)) 0))
             (map (lambda (x)
                    (list (path->string x) (rule1 (tokenize-with BSLLang-v02 x))))
                  file-list))))]

Функция @racket[return-file-list] возвращает список всех файлов существующих в указанном каталоге, а @racket[apply-rules] применяет правило @racket[rule1] к каждому найденному файлу и возвращает обработанные данные.
Все что остается это просто вывести на печать результат.

@racketblock[
 (define (print-result result-list)
   (for  [(file-content result-list)]
     (begin
       (printf "~nFile ~a" (car file-content))
       (for  [(elm (cadr file-content))]
         (printf "~nError at (~a ~a)"
                 (position-line (position-token-start-pos elm))
                 (position-col (position-token-start-pos elm)))))))]


В принципе все, осталось только написать обвязку работы  командной строкой.
Так как это не особо интересно с точки зрения решения, то пропущу этот момент. Желающие могут посмотреть текст в файле @filepath["src/part002/main.rkt"]


Компилируем @exec{raco exe  -o styleguide src/part002/styleguide.scm} (можно не делать - все будет работать из REPL, но если хочется исполняемый файл - то придется с командной строкой поработать), запускаем с указанием  директории @filepath["test-data"] (как помните у нас там лежит файл в который выводили тестовую строку) и видим - все работает
@interaction[#:eval my-eval
             (main '("--dir" "test-data"))
             ]

Видим в выводе список ожидаемых ошибок (строки 2 и 3).
Особо нетерпеливые могут проверить код xUnit


Отлично. Все работает как ожидалось. Помните мы отказались от регулярных выражений из-за сложности обработки правила с буквой Ё? Давайте посмотрим - насколько сложно добавить эту проверку сейчас.

@section{Второе правило. Буква Ё}

Стандарт гласит 
@t{
 1.1. В текстах модулях не допускается использовать букву "ё". 
}

Почему так и с чем связана нелюбовь к букве "ё" - не ясно, но правило есть и его можно реализовать.
Первая попытка реализации выглядит так
@racketblock[
 (define-tokens IDTokens (correctID inCorrectID))
 (define-lex-abbrevs
   [IDStart (:or #\ alphabetic)]
   [IDRest (:or #\ alphabetic (:/ #\0 #\9))]
   [ID (:seq IDStart (:* IDRest))])]

В этом коде мы определяем регулярное выражение для разбора идентификатора и в анализаторе - обработаем его
@racketblock[
(define BSLLang-v03
  [lexer-src-pos
   [(:seq (:+ #\Tab)) (if (= 0 (position-col start-pos))
                          (token-CorrectBOS lexeme)
                          (return-without-pos (BSLLang-v03 input-port)))]
   [(:seq (:* #\Tab) #\Space
          (:*
           (:or #\Tab #\Space)))           (if (= 0 (position-col start-pos))
                                               (token-inCorrectBOS lexeme)
                                               (return-without-pos (BSLLang-v03 input-port)))]
   [ID (if (or (string-contains? lexeme "ё")
               (string-contains? lexeme "Ё"))
           (token-inCorrectID lexeme)
           (token-correctID lexeme))]
   [(:seq #\/ #\/ (:* (:- any-char  #\NewLine)) #\NewLine) (return-without-pos (BSLLang-v03 input-port))]
   [(:seq #\" (:* (:or (:- any-char #\") (:seq #\" #\"))) #\") (return-without-pos (BSLLang-v03 input-port))]
   [not-WS (return-without-pos (BSLLang-v03 input-port))]
   [(eof) eof]])]

Как следует из определения анализатора - если прочитанная лексема соответствует регулярному выражению ID то анализируем - есть внутри прочитанного текста буква Ё. Если есть - возвращаем inCorrectID иначе correctID. Если в тексте встретился комментарий или строка - пропускаем.
Проверим 
@interaction[#:eval my-eval
             (tokenize-with BSLLang-v03 "Счётчик = ...;// в комментарии ё/Ё можно\nСчётчик = .... \"в строке ё/Ё тоже можно\"" )]

Давайте подумаем - сколько раз вызовется функция @racket[string-contains?] в переданном тексте будет 500 идентификаторов и ни одой буквы Ё? Правильно - ровно 1000 раз. Можно ли избежать ненужных проверок?
Смотрим код 

@racketblock[
(define-lex-abbrevs
  [IDStart-w-YO (:or #\_ alphabetic)]
  [IDStart-wo-YO (:- (:or #\_ alphabetic) (:or #\Ё #\ё))]
  
  [IDRest-w-YO (:or #\_ alphabetic (:/ #\0 #\9))]
  [IDRest-wo-YO (:- (:or #\_ alphabetic (:/ #\0 #\9))(:or #\Ё #\ё)) ]
  [ID-w-YO (:seq IDStart-w-YO (:* IDRest-w-YO))]
  [ID-wo-YO (:seq IDStart-wo-YO (:* IDRest-wo-YO))])



(define BSLLang-v04
  [lexer-src-pos
   [(:seq (:+ #\Tab)) (if (= 0 (position-col start-pos))
                          (token-CorrectBOS lexeme)
                          (return-without-pos (BSLLang-v04 input-port)))]
   [(:seq (:* #\Tab) #\Space
          (:*
           (:or #\Tab #\Space)))           (if (= 0 (position-col start-pos))
                                               (token-inCorrectBOS lexeme)
                                               (return-without-pos (BSLLang-v04 input-port)))]
   [ID-wo-YO (token-correctID lexeme)]
   [ID-w-YO (token-inCorrectID lexeme)]
           
   [(:seq #\/ #\/ (:* (:- any-char  #\NewLine)) #\NewLine) (return-without-pos (BSLLang-v04 input-port))]
   [(:seq #\" (:* (:or (:- any-char #\") (:seq #\" #\"))) #\") (return-without-pos (BSLLang-v04 input-port))]
   [not-WS (return-without-pos (BSLLang-v04 input-port))]
   [(eof) eof]])]

и понимаем что да. Как это работает? Очень просто - здесь определено два выражения для ID - одно содержащее в себе букву Ё а другое нет. И в зависимости от того как выражение сработало в анализаторе - вызывается возвращение лексеме с типом inCorrectID или correctID.
Проверим
@interaction[#:eval my-eval
             (tokenize-with BSLLang-v03 "Счетчик = ...;\nСчётчик = ...;// в комментарии ё/Ё можно\nСчётчик = .... \"в строке ё/Ё тоже можно\"" )]

Запомним этот трюк - далее его буду применять без объяснений.

@section{Умные мысли приводящие в уныние}

Смотрите - у нас уже 2 правила. А это значит что пора задуматься о

@itemlist[
 @item{Идентификации правил}
 @item{Документировании правил}
 @item{Стандартизации интерфейса использования правил}
 @item{Настройке использования правил}]

Веселая пора быстрых решений кончилась - осталась долгая и планомерная работа по разработке анализатора. Начнем конечно со бора требования.

