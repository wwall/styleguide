#lang scribble/manual
@(require scribble/eval)

@(define my-eval (make-base-eval))
@(my-eval '(require styleguide/src/common/common
                    styleguide/src/part001/part001
                    ))

@title{Попытка решение №1. Неправильная, но полезная для понимания}

В принципе эту часть можно не читать, если вы понимаете что будем делать в процессе разработки. Иначе - хотя бы пробежаться глазами стоит.

@section{Определение правила}

Для старта возьмем на разработку следующее правило -
@t{
 5. Текст модуля должен быть оформлен синтаксическим отступом. Для синтаксического отступа используется табуляция (а не пробелы, чтобы при смене числа знаков в табуляции выравнивание текста сохранялось).
 Размер табуляции - стандартный (4 символа).
}

Это простое правило которое говорит о том, что в начале строки должны быть только символы табуляции.
Напишем простое регулярное выражение, которое будет искать с начала строки последовательность содержащую в себе хотя бы один пробел - @RACKET[#rx"(?m:^((\t)*( )+))"]

@section{Общие данные}

Для простоты работы определим строку которую будем анализировать и поместим это определение в файл @filepath["common/common.rkt"].
Пусть в нашей строке будет следующий состав - 
@itemlist[
 @item{Первая строка - три символа табуляции и символ перевода строки. Считаем что эта строка полностью удовлетворяет требованию стандарта}
 @item{Вторая строка - 12 пробелов и символ перевода строки. Эта строка - не проходит проверку.}
 @item{Третья строка - один символ табуляции и 8 пробелов. Эта строка также не проходит проверку}
 @item{Четвертая строка - определение переменной и далее один символ табуляции и 8 пробелов. Эта строка проходит проверку, потому как последовательность пробелов и табов находиться не в начале строки}]

Само определение на языке @racket[DrRacket] выглядит так

@racketblock[
 (define (return-test-string)
   (string-append 
    (list->string '(#\Tab #\Tab #\Tab #\NewLine))
    (list->string '(#\Space #\Space #\Space #\Space #\Space #\Space #\Space #\Space #\Space #\Space #\Space #\Space #\NewLine))
    (list->string '(#\Tab #\Space #\Space #\Space #\Space #\Space #\Space #\Space #\Space  #\NewLine))
    (string-append "Переменная = 1" (list->string '(#\Tab #\Space #\Space #\Space #\Space #\Space #\Space #\Space #\Space  #\NewLine)))))
 ]

Тестовую строку написали таким сложным образом по одной простой причине - наглядно видно из чего она состоит.

Результат вычисления этой функции можно увидеть в @racket[REPL]

@interaction[#:eval my-eval
             (return-test-string)]

Получилась неплохая строка для теста.



@section{Реализация правила}

Приступим к определению самого правила. Так как мне надо просто показать путь решения, то решим задачу в лоб.
Самый простой вариант такой
@interaction[#:eval my-eval 
             (define ws-regexp #rx"(?m:^((\t)*( )+))")
             (regexp-match-positions* ws-regexp (return-test-string))]

В REPL виден результат выполнения поиска  по регулярному выражению. Но функция @racket[regexp-match-positions*] возвращает список содержащий описание
позиций где было найдено наше выражение в виде смещения от начала строки. Для вывода номера номера строки где найдена ошибка надо по смещению от начала потока вычислить номер строки.
Так как эффективность и скорость кода пока не интересует - не думаем о производительности и просто считаем сколько переводов строки встретилось от начала потока до указанного смещения.
@racketblock[
 (define (offset-in-line string position)
   (length (regexp-split #rx"\n" string 0 position)))]

Функция @racket[regexp-split*] делит входную строку на список строк. Делителем является перевод строки. А функция @racket[length] считает количество получившихся строк. Аргумент position просто указывает до какой позиции надо читать анализируемую строку.

Проверим функцию offset-in-line, например для смещения 4

@interaction[#:eval my-eval 
             (offset-in-line (return-test-string) 4)]

Все успешно работает, и теперь можем собрать наше определение для правила rule1
@racketblock[
 (define (rule-1 string)
   (map (lambda (x) (offset-in-line string (car x)))
        (regexp-match-positions* ws-regexp string)))]

Этот код должен быть вполне понятен, но еще раз объясню - процедура @racket[rule-1] принимает строку для анализа, применяет регулярное выражение @racket[#rx"(?m:^((\t)*( )+))"] к ней, а затем вычисляет номер строки в которой правило сработало.

@interaction[#:eval my-eval
             (rule-1 (return-test-string))]

Вполне ожидаемый результат учитывая логику построение тестовой строки.


Что бы каждый раз не вычислять в REPL результат - определим тест и будем пользоваться всеми благами модуля @racketmodname[rackunit].
Наш тест ожидает при анализе строки будет возвращен список с номерами строк в которых не выполняется правило стандарта. Тест и определение тестовой строки поместим  в этом же файла

@racketblock[
 (module+ test
   (require rackunit)
   (check-equal? (rule-1 (return-test-string)) '(2  3)))]

Компилируем и видим что все сработало как и ожидалось.

@section{На пути к неудаче}

Сейчас кажется что исползуя регулярные выражения мы можем определить и следующие правила. Ок, давайте попробуем реализовать  достаточно простое правило 

@t{
 1.1. В текстах модулях не допускается использовать букву "ё". 
}

Тут много думать не надо - регулярное выражение очень простое @RACKET[#rx"Ё|ё"], 

@interaction[#:eval my-eval
               (require rackunit)
               (define Ё-regexp #rx"Ё|ё")       
               (define (rule-2 string)
                 (map (lambda (x) (offset-in-line string (car x)))
                      (regexp-match-positions* Ё-regexp string)))
               (check-equal? (rule-2 "Счётчик = 1") '(1))]

На первый взгляд - работает, но что будет если буква Ё встретится в комментарии или в опеределении строки? 

@interaction[#:eval my-eval
             (check-equal? (rule-2 "Счетчик = 1 // буква Ё в комментарии может быть") '())
             (check-equal? (rule-2 "Счетчик = #\" буква Ё в строке тоже может быть#\"") '())
]

              
Невооруженным взглядом видно что не работает. Конечно, можно написать более сложное регулярное выражение (TODO доделать его), которое будет учитывать положение буквы Ё (в строках и комментариях стандарт разрешает её писать).
@margin-note*{Известная шутка про  @link["https://xkcd.com/1171/"]{регулярки}}
Но это очень долгий путь - ведь дальше будут более сложные требования к коду и уже сейчас понятно - регулярные выражения этот не тот инструмент который следует применять для решения этой задачи.
              

