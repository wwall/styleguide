#lang scribble/manual
@(require scribble/eval)

@require[@for-label[styleguide
                    racket/base
                    rackunit
                    parser-tools/lex
                    ]]

@(define my-eval (make-base-eval))
@(my-eval '(require styleguide/src/common/common
                    styleguide/src/part002/part002
                    styleguide/src/part002/main
                    ))


@section{Планирование и сбор требований.}

@subsection{Требования к правилам}
@itemlist[
 @item{Необходимо именовать наборы правил. Т.е. иметь отдельные наборы правил для разных ситуаций проверки - например отдельно набор для проверки рабочего код и кода тестов}
 @item{Аргументы - у правил могут быть аргументы. Необходиоо уметь описывать их}
 @item{Имя правила - идентификатор}
 @item{Документация - краткое описание правила}]
@subsection{Требования к обрабатываемым файлам}
@itemlist[
 @item{Маска имени файла}
 @item{Список путей для которых применяется маска}
 @item{Конкатенация списков описывающих маску и список путей}]

@subsection{Требования к обработке правила}

@subsection{Место вызова анализатора}
Командная строка. 
@subsection{Выходные данные.}
Строка с описанием результата проверки 

@subsection{Первый вариант файла.}
Формат представления данных - YAML. Для первого запуска файл выглядит так
@nested[#:style 'inset ]{
@verbatim|{code+test:
  paths:
  - files:
      name: *.bsl
      path: [src]
  - files:
      name: *.bsl
      path: [test]
  checks:
  - check-BOS:
      arg: {}
  - check-cyrillic-YO-letter:
      arg: {}
 }|
 }

Этот файл описывает настройку для проверки с именем @racket{code+test}, которая проверит все файлы с расширением "bsl" из каталогов "src" и "test" с помощью правил @racket{check-BOS}
(проверка начала строки) и @racket{check-cyrillic-YO-letter} (проверка наличия буквы Ё в идентификаторах)

