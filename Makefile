scrbl := scribblings/index.scrbl

www := public

all: html-single


html-single: $(scrbl)
	raco scribble \
		--html \
		--dest $(www) \
		--dest-name index.html \
		--redirect-main https://docs.racket-lang.org/ \
		\
		$(scrbl)


